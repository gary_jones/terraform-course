# Create a basic ALB
resource "aws_alb" "my-app-alb" {
  name = "my-app-alb"
  subnets         = [aws_subnet.main-public-1.id, aws_subnet.main-public-2.id, aws_subnet.main-public-3.id]
  security_groups = [aws_security_group.alb-securitygroup.id]
  load_balancer_type = "application"
  internal = false
}

# Create target groups with one health check per group
resource "aws_alb_target_group" "target-group-1" {
  name = "target-group-1"
  port = 80
  protocol = "HTTP"
  vpc_id = aws_vpc.main.id

  target_type = "instance"

  lifecycle { create_before_destroy=true }

  health_check {
    path = "/"
    #port = 2001
    healthy_threshold = 6
    unhealthy_threshold = 2
    timeout = 2
    interval = 5
    matcher = "200"
  }
}

//resource "aws_alb_target_group" "target-group-2" {
//  name = "target-group-2"
//  port = 80
//  protocol = "HTTP"
//
//  lifecycle { create_before_destroy=true }
//
//  health_check {
//    path = "/api/2/resolve/default?path=/service/my-service"
//    port = 2010
//    healthy_threshold = 6
//    unhealthy_threshold = 2
//    timeout = 2
//    interval = 5
//    matcher = "200"
//  }
//}

# Create a Listener
resource "aws_alb_listener" "my-alb-listener" {
  default_action {
    target_group_arn = "${aws_alb_target_group.target-group-1.arn}"
    type = "forward"
  }
  load_balancer_arn = "${aws_alb.my-app-alb.arn}"
  port = 80
  protocol = "HTTP"
}

# Create Listener Rules
resource "aws_alb_listener_rule" "rule-1" {
  action {
    target_group_arn = "${aws_alb_target_group.target-group-1.arn}"
    type = "forward"
  }

  condition {
    path_pattern {
      values = ["/"]
    }
  }

  listener_arn = aws_alb_listener.my-alb-listener.id
  priority = 100
}

//resource "aws_alb_listener_rule" "rule-2" {
//  action {
//    target_group_arn = "${aws_alb_target_group.target-group-2.arn}"
//    type = "forward"
//  }
//
//  condition { field="path-pattern" values=["/api/2/resolve/default"] }
//
//  listener_arn = "${aws_alb_listener.my-alb-listener.id}"
//  priority = 101
//}

resource "aws_launch_configuration" "example-launchconfig" {
  name_prefix     = "example-launchconfig"
  image_id        = var.AMIS[var.AWS_REGION]
  instance_type   = "t2.micro"
  key_name        = aws_key_pair.mykeypair.key_name
  security_groups = [aws_security_group.myinstance.id]
  user_data       = "#!/bin/bash\nsudo -s\namazon-linux-extras install epel -y\nsudo yum install nginx -y\nMYIP=`ifconfig | grep 'inet 10' | awk '{ print $2 }'`\necho 'this is: '$MYIP > /usr/share/nginx/html/index.html\nservice nginx start"
  lifecycle {
    create_before_destroy = true
  }
}


# Create an ASG that ties all of this together
resource "aws_autoscaling_group" "my-alb-asg" {
  name = "my-alb-asg"
  min_size = "2"
  max_size = "2"
  launch_configuration = "${aws_launch_configuration.example-launchconfig.name}"
  health_check_grace_period = 300
  health_check_type         = "ELB"
  force_delete              = true
  //availability_zones = ["${var.AWS_REGION}a", "${var.AWS_REGION}b", "${var.AWS_REGION}c"]
  vpc_zone_identifier       = [aws_subnet.main-public-1.id, aws_subnet.main-public-2.id]

//  depends_on = [
//    "aws_alb.my-app-alb",
//  ]

  // For the ELB (classic) use the load_balancers attribute instead
  target_group_arns = [
    "${aws_alb_target_group.target-group-1.arn}",
    #"${aws_alb_target_group.target-group-2.arn}"
  ]

  lifecycle {
    create_before_destroy = true
  }
}
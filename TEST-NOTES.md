Next
----
Do LATER
SECTION 9: Module development demo-18b - showing how to use profiles etc...for environment specific builds.

(84) AWS CodePipeline Section 10 :
CodeCommit: git repository

CodeBuild: Launch EC2 to run your test/build phase. Can also do Docker builds

CodeDeploy: Deploys on EC2, Lambda and ECS

All can be done from Jenkins



In general for each demo
-------------
terrform init - setup
terrform plan - check it
terraform apply - do it
terraform destroy - remove all things done in apply - NB it may not always remove everything. ALWAYS CHECK
Add this so that you don't have a command line approve --auto-approve

DEMOS
-------

Basic Ideas
----
demo-1 - basic setup with accesskey and secret key to be able to allow access to AWS to perform build steps
         - Also demonstrates install of public/private key for ssh access to instance
         The pub key is hard coded as a variable in this demo
         - NB With demo 1 you will not be able to ssh onto the instance you have created because the security groups have not been setup
         As a simple test, modify the default security group temporarily to allow ssh and you should then be able to 
         access the instance using ssh -i gary ec2-user@<ip>
         
demo-2 - NB This demo did not work out of th ebox. I needed to comment out the loop in the script.sh file
         Software provisioning by loading and executing script onto ec2 instance
         - Just installs nginx onto instance and starts it up look at port 80 (security group) on the IP provided in output
         - NB Because the public key is now in a file we use file(var.PATH_TO_PUBLIC_KEY) to refer to it
         rather than just the hardcoded content of the pub key file
         - Also shows how we can echo useful info like the IP of the remote instance - see "local-exec" in instance.tf
           which puts the private IP of instance into a local file
         - Also shows the use of "output" in instance.tf to show how to print info about the instance when doing "apply"
         - Again NO security groups are set up so out of the box you will not be able to ssh onto the instance

demo-3 - Only slight variations on 2. Don't bother
demo-4 - Only slight variations on 2. Don't bother
demo-5 - Setup of single security group allowing inbound access from anywhere in eu-west-2
         This demo shows how you can setup AWS Security Groups to allow access from other AWS services using IP ranges
         NB See https://www.terraform.io/docs/providers/aws/d/ip_ranges.html
         This shows how IP ranges can be retreived for various AWS services
         
demo-6 - Uses default VPC and setup consul with 3 instances
         Shows how to use modules from remote sources - one that was created for the course
         This demo does not show anything other than how to use modules. It just happens to use a consul cluster 
         setup as the demo. You can ssh onto the output address using:
         ssh -f gjkey ubuntu@<address> but there is nothing
         much to see other than the consul server running there.
         Downloads the modules to .terraform/modules
         ssh-keygen -f gjkey for key
         Installs Consul on the instance: see the Git repo for the consul module :https://github.com/wardviaene/terraform-consul-module?ref=terraform-0.12
         Starts 3 Consuk instances consul-0,1 and 2. 

Infrastructure setup - START at demo 7 FOR BASIC NETWORK SETUP
----
demo 7 - New AWS VPC, IG, NAT and subnets
         ssh-keygen -f gjkey for key
         For destroy check the following are gone:
         1. VPC
         2. Subnets
         3. Route Tables
         4. Key Pairs
         
demo 8 - Add EC2 instance and security groups allowing inbound SSH connections to ec2 (also added an Elastic IP association with instance)
         ssh-keygen -f gjkey
         See aws_eip lb for elastic ip setting in instance.tf
         connect using ssh -i gjkey ec2-user@<address>
         
demo 9 - Add EBS in an availability zone to an instance
For any issues destroying the infrastructure in this demo with the mounted file system you need to terminate the ec2 instance in
the console first before re-running the destroy command.

         ssh-keygen -f mykey
         sizes are in GiBs (1024 to power 3 bytes) - Gigabytes
         For device naming see https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/device_naming.html
         ssh -i mykey ec2-user@<address>
         
         EBS - (36)
         Elastic Block Storage - AWS service - removed from instance when instance terminates
         Some instances have a root EBS volume by default (t2 micro)
         Change root size on instance - use root_block_device block on instance resource
         Local storage - ephemeral storage - lost when instance terminates
         
         After EBS is create the filesystem needs to be created and mounted:
         Manual Steps
         - ssh onto instance 
         - check filesystem df -h to see existing ones
         - check console to see that new EBS is available on the instance
         - sudo -s as you need to be root to do this
         - mkfs.ext4 /dev/xvdh - to create FS
         - mkdir /data
         - mount /dev/xvdh /data
         
         At this point when you reboot the volume will be gone again so need to add volume into /etc/fstab
         /dev/xvdh /data ext4 defaults 0 0
         
         now if you umount /data, you can just mount /data because of this entry
         
         NB One issue - if we terminate the instance and recreate then the change will be gone because /etc/fstab is based on /etc and / will be gone
         Use user data to add the line into fstab
         
         Can snapshot EBS volumes to keep data

         
         
demo 10 - User data and cloud-init - see https://cloudinit.readthedocs.io/en/latest/topics/format.html
For any issues destroying the infrastructure in this demo with the mounted file system you need to terminate the ec2 instance in
the console first before re-running the destroy command.
Alternative solution (DOES NOT SEEM TO WORK) -  solved in terraform script with "destroy" event on EBS volume:provisioner "remote-exec". 
Has to be on volume provisioner as the destory happens on this first

In cloudconfig.tf the content_type defines the type of script :
See https://cloudinit.readthedocs.io/en/latest/topics/format.html for how to setup cloudinit.tf
init.cfg is a cloud-config script - see https://cloudinit.readthedocs.io/en/latest/topics/examples.html

See lvm commands used in volumes.sh which allow you to create and mount filesystems. This script also updates /etc/fstab
vgchange
pvcreate
vgcreate
lvcreate

pvdisplay
lvdisplay to see properties of a mount when on the instance

This means we don't need to do the manual mounting steps setup in demo 9

USER DATA (lecture 38 and 39)
Customisation at launch time - not reboot or restart - only launch.
- Install software or prepare instance to join cluster.
- Execute commands
- Mount volumes

User data can be specified as :
- As string for simple commands (basic bash script e.g.)
- Template system of terraform as in the example. See variable in instance.tf 

E.g. from demo 16 later on
user_data       = "#!/bin/bash\napt-get update\napt-get -y install nginx\nMYIP=`ifconfig | grep 'addr:10' | awk '{ print $2 }' | cut -d ':' -f2`\necho 'this is: '$MYIP > /var/www/html/index.html"

See : https://www.terraform.io/docs/providers/template/d/cloudinit_config.html

demo 11 - Route53 setup See notes below (41)

demo 12 - RDS on the example instance install the mysql client on the ec2 instance after started up:
sudo yum update -y
sudo yum install mysql
mysql -u root -h mariadb.cwxkrirwjz4g.eu-west-2.rds.amazonaws.com -p'<password created at startup>'
get host from terraform output

demo 13 - IAM Users - Creates some users and assigns them permissions through iam policy in groups
Changed policy from being an AdministratorAccess one so that when you do terraform destroy ot does not
clean up existing admin users and groups

demo 14 - IAM Roles
Creates EC2 instance and assigns a role to it allowing it access to S3


demo-15 - Auto scaling - Just proves the auto scaling policy - does not launch with load balancing
See Note AUTO SCALING (48 and 49) about how to trigger this policy
NB in autoscaling.tf for aws_autoscaling_group
health_check_type         = "EC2"

demo-16-gj - load balancing with ELB and auto scaling
NB in autoscaling.tf for aws_autoscaling_group
  health_check_type         = "ELB"
  load_balancers            = [aws_elb.my-elb.name]
  
  for aws_launch_configuration
  user_data = "script to launch new nginx on every launch"

demo-16alb-gj - load balancing with ALB and auto scaling
NB
Difference between ELB and ALB
"aws_autoscaling_group" "example-autoscaling"
ELB uses "load_balancers" but ALB uses target_group_arn

demo-16alb-gj-alternative - set up as alternative as the previous demo didn't work at one point. Now both work and are effectively very similar
THIS WORKS TOO

demo-16alb-gj-alternative-MultiService - also works (use as the basis of multi service apps)

demo-16alb-gj-alternative-MultiService-autoscaling - same as previous with auto scale policies also works (use as the basis of multi service apps)

NB Occasionally with ALB I've noticed that one of the instances does not get registered with the load balancer.
To fix this, terminate the offending instance and the ALB will just register a new one (assuming you have a minumum size set to 2)

demo-17-gj - Elastic Beanstalk

demo-18-gj
Variables - use terraform apply -var ENV=dev to run a dev instance up (defaults to PROD) - this will terminate prod and run DEV.
In reality you have terrafrom state file for prod and dev or use modules to seperate environment details (this is whta you should do)

demo-18b - MODULES

packer-demo-gj
brew install packer first.
dpkg -l | grep docker or nginx (to check installs have worked)
or
yum list | grep docker etc...

jenkins-packer-demo-gj
NB Keeping this one in eu-west-1 and using the ubuntu image required user ubuntu@instance

Docker demo
-----------
docker-demo-1 for terraform to create Elastic Container Registry
    - terraform apply

docker-demo - for the sample application (could be java)
    - run docker build -t 245475869541.dkr.ecr.eu-west-2.amazonaws.com/myapp:1 .
    - aws ecr get-login (put command in $() to execute immediatley to login to the ECR)
    - $(aws ecr --no-include-email get-login)
    - docker push 245475869541.dkr.ecr.eu-west-2.amazonaws.com/myapp:1
    - My AMI - 245475869541.dkr.ecr.eu-west-2.amazonaws.com/myapp:1
    
docker-demo-2
    cp tfstate from docker-demo-1
    create mykey using ssh-keygen
    terraform apply
     
    If you docker destroy - you will need to terraform apply in docker-demo-1 first to re-create the ECR
    Make sure you lookup the AMI ID from http://docs.aws.amazon.com/AmazonECS/latest/developerguide/ecs-optimized_AMI.html
    This AMI will have ECS binaries, docker etc... already installed
    
See docker-demo-2-java for a springboot app being deployed. Just using springboot-2-oidc
docker build -t 245475869541.dkr.ecr.eu-west-2.amazonaws.com/myapp:1 .
docker push 245475869541.dkr.ecr.eu-west-2.amazonaws.com/myapp:1
docker run -p 8000:8080 7001d23793ee (imageid)
http://192.168.99.100:8000/cpas for testing

    

DEMO 14 NOTES
==============
On EC2 instance :
- sudo -s
- yum install ptyhon3
- yum install python3-devel
- pip3 install awscli
- now should have "aws" command
- aws s3 ls s3://mybucket-c29df1-gj
- vi test.txt
- aws s3 cp test.txt s3://mybucket-c29df1-gj/test.txt

Roles are a much safer way to provide access to services rather than access key and secret key. They change periodically.
Where do credentials come from on the instance?
To view meta-data there is an AWS URL : curl http://169.254.169.254/latest/meta-data
Shows list of meta-data folders: 
curl http://169.254.169.254/latest/meta-data/iam shows info and security-credentials
curl http://169.254.169.254/latest/meta-data/iam/security-credentials/ shows s3-mybucket-role
shows
{
  "Code" : "Success",
  "LastUpdated" : "2020-04-21T13:18:01Z",
  "Type" : "AWS-HMAC",
  "AccessKeyId" : "ASIATSJ4AQNSVMCRFXTI",
  "SecretAccessKey" : "0VJzx8xOEad39JJru/3a+Kt4k070BxsDnFgdJblA",
  "Token" : "IQoJb3JpZ2luX2VjEAYaCWV1LXdlc3QtMiJHMEUCIC0mH3ZwexM/teXQ7Gpy+J35vTNhlrfIeEkwLo3x+zo4AiEAjQ8EYCRRc9NgQLB/zIqWGG5Jnx15BrzNd/uizWxLD1kqtAMILhAAGgwyNDU0NzU4Njk1NDEiDPKWdGvCUeESi6uKUCqRA/PHrfT8P47243VmGQyzxuwqLC9vfZThekLmzV674XFTgzO3ix6vNTXzgNjWEqAqxdN7mQDvy59Ew8A+HwJuCuSgCDhPY3Lp+ASXtQYL1p3EkriGJsNiYxNFLTRIdz99iSgGN8j2JFpevvF5eGAyJpJ+5vLMMq5n8xlvKth/JA5fMMBXG6yv5kArapFeEIwUGzSil1vt2GXV3zJz8Rm67NVEySPx9Fc6aSvPfVbYoM00fa2xE5r26oUjGQ0JnKtbB8SXay+LfOqA6FcttdCdIJa71vSmqc09sb0CMU1Gj24uvohaDYJhv1IMa4V0X3Rnkgu9G/e/rsD9zZQgsm+TRzFhoZ7wGsVkKr6whUiyqXvxoCQqRgtjv2d6m6n31rdbxjyrpZzIrerOp+fkYJiRPWbBaNpvvE8J1N+xVx6iCdoXrxxFzxYLmEPPzMHVlsL2CG0rd+PBUQNm7a4BbBmii+GYc6YyrKFdkQJRHDsxB8G8fykJiAXuW2g+Vh+2ep6IMbwnsrnjQre3vGZO9biz3wfvMM/k+/QFOusBTWm0nPtlPRPUW+SNAq0tMVppgRkxv9Vsy4l/u59/RNMIgX4UxPsRLHB68V6vZlz9DWl/BSpZ9Hbw8jMLSQmmJGVvAFIv/aQF45mcVGz/1yx1QI78s8PmXc1msIenLEHiP1gSQ6cjGGBdIHW3zrnj7RUYdTuwCT1TNtDy/03MKenLh+O7GOEKtIkj2zEJqqMi3W8Yck8ytROoJmykixf30zaV4aDSbpoQh1p+Wqx4QyywLX1O1iJjpA9GJARpAXN3ZT8ER4NeTvLD/0VkKuvxVBSsPn+yo4iV8tLxgCNoSXy7abRRxJwlt7YhKQ==",
  "Expiration" : "2020-04-21T19:52:03Z"
}
==============


LOGS - can see logs in tail -f /var/log/cloud-init-output.log on the instance

PUBLIC/PRIVATE Keys - for all demos just use: ssh-keygen -f gjkey

CONNECT TO EC2 : ssh -i <privatekeyfile> ec2-user@<IP>

Latest can be obtained from this repo: https://github.com/wardviaene/terraform-course

Add terraform.tfvars added to .gitignore so it is never added to repo.

Installing software - 
 1. Use Packer to create an AMI
 2. Use standard AMI and install software on it using file uploads, remote exec and automation tools like chef, puppet and ansible


COMMANDS:
terraform init - initialise directory
terraform plan -out tf.plan.out - export the current plan from the .tf files in current directory to a file
terraform apply - apply the current plan from the .tf files in the current directory
terraform apply tf.plan.out - apply the plan save in a file
terraform show - show the current plan
terraform destroy - drop everything from the current applied infrastructure
terraform graph -module-depth=1 | dot -Tpng | open -a Preview -f
terraform output - display any output steps in script

terraform get - download any additional modules with remote "source" values to local .terraform/modules directory - e.g. see demo-6-gj/modules.tf

aws configure to set up local credentials for use by aws commands and other programs like terraform that need access to things like s3


terraform plan - will show changes that will be applied to the current environment - 
ie. if nothing exists yet is should show you everything
once you have run apply, if you run plan again it shouls say "No changes. Infrastructure is up-to-date."
only changes applied to tf files will appear in plan after this so save them as files to be applied in a similar way to liquibase

SYNTAX:
VARIABLES:
if you ommit the type then terraform will infer the type from the attribute 
string
variable "a-string" {
  type = string
}
number (integer)
variable "a-number" {
  type = number
}

bool
variable "a-boolean" {
  type = bool
}

COMPLEX:
list(type) - type can be string, number and bool, always maintains order added to list
[1,2,3,4,5]

set(type) - does not allow duplicates and sorts on output

map(type)
{"key":"value"}
object({list of attr name=types}) - like a map but each attribute can have a different type
{
  firstname = "john"
  housenumber = 10
}

tuple([list  of types]) - like a list but can have types for each element
[1, "gary", false]

TEMPLATE PROVIDER (24)
in AWS you can pass commands that need to be executud for the first time when the instance starts
In AWS this is called "user-data" - 
If you want to pass user-data (section 38) that depends on other information (e.g. IP Addesses) you can use the provider template

ADDITIONAL PROVIDERS:
- AWS
- Google Cloud
- Azure
- Heroku
- Digital Ocean

On Premise / private cloud:
- VMWare vCloud /vSphere/OpenStack 

- Addtional providers include
    - GitHub
    - DataDog
    - Mailgun
    - DNSSimple/DNSMadeEasy/UltraDNS - DNS Hosting
Full list of providers is at - https://www.terraform.io/docs/providers

MODULES (26)

AWS (30)
AWS - there is a default VPCAWS_ACCESS_KEY - always created by defaul if not specified. Isolates interfaces



STATIC IPs and DNS (41)
private IP can be specified in script so that an instance always gets same IP if you want.
Rather than using IP addresses use host names (Route53)
 - Register domain name using AWS or other registrar
 - Create a zone in route53 (e.g. example.com)
 - Add DNS records (e.g. server1.example.com)
 - When you register your domain name you need to add the AWS name servers to the domain
 - AWS has a lot of name servers.
 - Use output resource to output the property aws_route53_zone.example-com.name_servers
 
test:
host server1.newtech.academy ns-12.awsdns-01.com (choose the ns from the printed output after terraform apply)
host -t MX server1.newtech.academy ns-12.awsdns-01.com - should see server1.newtech.academy has no MX record
host -t MX newtech.academy ns-12.awsdns-01.com sees:
newtech.academy mail is handled by 1 aspmx.l.google.com.
newtech.academy mail is handled by 10 aspmx2.googlemail.com.
newtech.academy mail is handled by 10 aspmx3.googlemail.com.
newtech.academy mail is handled by 5 alt1.aspmx.l.google.com.
newtech.academy mail is handled by 5 alt2.aspmx.l.google.com.

AUTO SCALING (48 and 49)
Auto scaling groups created to automatically add and remove instances
need 2 resources:
1. AWS launch config - specifies properties of instance to be launched (AMI ID, security group etc..)
2. Auto scaling group (scaling properties min, max and health checks)

Once Auto scaling groups setup a Auto Scale policy
    - triggered based on threshold (CloudWatch alarm)
    - An adjustment will be executed

To install "stress" on the linux instance to trigger CPU thresholds for testing:
sudo amazon-linux-extras install epel -y
sudo yum install stress -y
then run this to stress cpu for 300 seconds
stress --cpu 2 --timeout 300

ELB (51)
To install nginx (all done as part of user_data in autoscaling.tf):
sudo amazon-linux-extras install epel -y
sudo yum install nginx -y
sudo service nginx start
#!/bin/bash\nsudo -s\nMYIP=`ifconfig | grep 'inet 10' | awk '{ print $2 }'`
echo $MYIP
sudo echo 'this is: '$MYIP > /usr/share/nginx/html/index.html

ALB (own project demo-16alb-gj)

(54) Elastic Beanstalk
AWS PaaS to maintain unserlying infrastructure
based on amazon linux
you are stil responsible for EC2 instances but, AWS will provide updates to apply
handles load balancing - load balancer and auto scaling.
Similar to Heroku
Lots of languages available includeing Docker (single or multi container using ECS - EC2 container service)
You get a CNAME and can then use Route53 to the CNAME
Once EB is running you can deploy using the EB utility from 
install EB on Mac:
brew update
brew install awsebcli
eb --version

solution_stack_name defined in elasticbeanstalk.tf can be found here for Java
https://docs.aws.amazon.com/elasticbeanstalk/latest/platforms/platforms-supported.html#platforms-supported.javase

RE-WRITE NGINX RULE - sed -i '/:80/ a rewrite ^.*$ /index.html;' /etc/nginx/nginx.conf
service nginx restart

TODO
(56) Advanced Terraform Interpolation (see https://www.terraform.io/docs/configuration/expressions.html)
${} used to refer to variables etc...

String var.name - string
MAP var.map["key"] or ${lookup(var.AMIS, var.AWS_REGION)}
List var.LIST, var.LIST[i] or ${var.subnets[i]} or ${join(",",var.subnets)}
output of module - module.NAME.output e.g. ${module.aws_vps.vpcid}
Count infor count.field ${count.index} for index
Path info path.TYPE e.g. path.cwd, path.module and path.root
Meta info terraform.FIELD e.g. terraform.env shows active workspace

Math: 
add(+), subtract(-), multiply(*) and divide(/) for float
Math: add(+), subtract(-), multiply(*), divide(/) and modulo (%) for integer

Conditionals: 
condition?trueval:falseval
Equality == !=
Numerical comparison >, <, >=, <=
Boolean loggic &&, ||, unary !


Packer and Jenkins Section 7
NB The versions of jenkins, packer etc... sometimes change and the install will fail.
To Check - After running the initial terrafom - ssh -i mykey ubuntu@<IP> 
and look at /var/log/clour-init-output.log for success or failure.
You may need to destroy everything and start again.
 
NB The example terraform creates both the jenkins instance and also the application instance.
The reason the app instance is not created initially is that the count on the app instance is 0 which leaves it up to the
Jenkins build to create more instances

These demo scripts do everything in one set of scripts plus a whole load of manual steps in Jenkins but,
Really this should be a script to 
1. Create Jenkins in a single terraform module (needed to update jenkins version in vars.tf)
2. Run Packer to create the application AMI (used https://github.com/wardviaene/packer-demo) as the packer-demo. Needed to make sure that the default VPC existed in Ireland (eu-west-1) before running.
NB In this example, the application is already bundled in as part of this packer build so there is no app deployment
So, 3 and 4 here are one step in the demo
3. Run Jenkins to build the application
4. Run terraform to build the infrastruture from a separate terraform module and deploy the apps to instances as part of this
Used this as the Git repo for the terraform-apply build step. https://github.com/wardviaene/terraform-course
5. Deploy the application to each instance (as part of 4)


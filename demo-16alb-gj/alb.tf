resource "aws_alb" "my-alb" {
  name            = "my-alb"
  subnets         = [aws_subnet.main-public-1.id, aws_subnet.main-public-2.id, aws_subnet.main-public-3.id]
  security_groups = [aws_security_group.alb-securitygroup.id]

  # optional - this is attached to an auto scaling group but,
  # you could specify the instances here instead if you didn't use an ASG
  #instances = [aws_instance.example-instance.id]

  enable_cross_zone_load_balancing = true
  tags = {
    Name = "my-alb"
  }
}

resource "aws_alb_listener" "frontend-listeners" {
  load_balancer_arn = aws_alb.my-alb.arn
  port = 80
  protocol = "HTTP"
  default_action {
    target_group_arn = aws_alb_target_group.frontend-target-group.arn
    type = "forward"
  }
}

resource "aws_alb_target_group" "frontend-target-group" {
  name = "alb-target-group"
  port = 80
  protocol = "HTTP"
  vpc_id = "${aws_vpc.main.id}"
}

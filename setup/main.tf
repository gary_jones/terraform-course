variable "region" {
  type = string
  default = "eu-west-1"
}

provider "aws" {
  region = var.region
}

resource "aws_s3_bucket" "app" {}

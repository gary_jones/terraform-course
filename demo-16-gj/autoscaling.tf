resource "aws_launch_configuration" "example-launchconfig" {
  name_prefix     = "example-launchconfig"
  image_id        = var.AMIS[var.AWS_REGION]
  instance_type   = "t2.micro"
  key_name        = aws_key_pair.mykeypair.key_name
  security_groups = [aws_security_group.myinstance.id]
  user_data       = "#!/bin/bash\nsudo -s\namazon-linux-extras install epel -y\nsudo yum install nginx -y\nMYIP=`ifconfig | grep 'inet 10' | awk '{ print $2 }'`\necho 'this is: '$MYIP > /usr/share/nginx/html/index.html\nservice nginx start"
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "example-autoscaling" {
  name                      = "example-autoscaling"
  vpc_zone_identifier       = [aws_subnet.main-public-1.id, aws_subnet.main-public-2.id, aws_subnet.main-public-3.id]
  launch_configuration      = aws_launch_configuration.example-launchconfig.name
  min_size                  = 2
  max_size                  = 2
  health_check_grace_period = 300
  # These next two lines are the difference if you have an ELB with autoscaling compared to individual instances with autoscaling
  health_check_type         = "ELB"
  load_balancers            = [aws_elb.my-elb.name]

  force_delete              = true

  tag {

    key                 = "Name"
    value               = "ec2 instance"
    propagate_at_launch = true
  }
}


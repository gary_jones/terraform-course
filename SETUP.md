AWS Notes
If for any reason you lose you default VPC etc. in AWS in any of your regions you can
restore default VPC, subnets etc...https://docs.aws.amazon.com/vpc/latest/userguide/default-vpc.html#create-default-vpc
Just re-create a default VPC.

In the AWS Console setup an IAM user with Admin priviledges here:https://console.aws.amazon.com/iam/home?region=eu-west-2#/users
Click on the user and go to the security credentials tab to create that user an access key. 
Use this access key and secret key in Terraform

Settings you are asked to provide on the command line go into these locations to allow access to AWS
Use : aws configure to set these up
Access and Secret keys: ~/.aws/credentials (Access Keys)
Other settings: ~/.aws/config

You can have names profiles in these files.
You can set the profile to use by either:
1. export AWS_PROFILE=<profile>
2. specify --profile <profile> as part of an AWS command line
3. in provider.tf specify "profile" under "aws". NB this can be a var as usual or left as a blank var so the user has to specify it.

Public Keys and Private Keys
To allow access via ssh to EC2 instances you will need to upload a public key to AWS.
This is specified in terraform via the public_key entry in the aws_key_pair config.
See demo-1-gj instance.tf and vars.tf (hardcoded public key just copied out of gary.pub)

Setting up variables:
1. Use terraform.tfvars
2. hard code in terraform files (no)
3. Using environment variable:
export TF_VAR_name (has to be case sensitive - use upper case)
e.g. export TF_VAR_AMI=ami-00a1270ce1e007c2
refer to it in the app as a var in vars.tf:
variable "AMI" {
  type = string
}

then use as normal e.g.
resource "aws_instance" "example" {
  ami           = var.AMI
  .
  .

Access Key and Secret Key
These are required for executing terraform commands in a your account.
You set these up in your account in the IAM console under a user in security credentials

NB For access key and secret key see :  https://terraform.io/docs/providers/aws/index.html 
- Not specifying any credentials at all will throw an error
1. Add empty variable "AWS_ACCESS_KEY" {} variable "AWS_SECRET_KEY" {} which forced terraform to ask for them if it can't find them
   see demo-3 - NB Variables that are not provided will be requested.
2. create a terraform.tfvars file containing the keys as well 1. - see demo-1-gj
3. add an ~/.aws/credentials file which means you won't be asked for them anymore so don't add 1. at all.
in this file you can set a [default] entry - you can specify 

# Setting up your environment with an IAM user to use for command line AWS cli and terraform commands
1. Create an IAM Group with 0 permissions
2. Create an IAM user belonging to the new group
3. Setup an access key on the user and copy into your .aws/credentials  (without this you will see Unable to locate credentials. You can configure credentials by running "aws configure".)
3. Follow this https://meirg.co.il/2021/04/23/determining-aws-iam-policies-according-to-terraform-and-aws-cli/
to determine which permissions your terraform user will need to run commands and add them one at a time. Without these you will see 
e.g. for aws s3 ls an error like "An error occurred (AccessDenied) when calling the ListBuckets operation: Access Denied"
4. Then you can run Terraform commands etc...
5. Rotate access keys https://rakeshjain-devops.medium.com/updating-aws-access-keys-using-awscli-7d107493b629

# I am Live steps runs in terminal
docker run -p 80:10080 -p 443:10080 --name iamlive-test -it unfor19/iamlive-docker --mode proxy --bind-addr 0.0.0.0:10080 --force-wildcard-resource --output-file "/app/iamlive.log"
# Runs in the background ...
# Average Memory Usage: 88MB
export HTTP_PROXY=http://127.0.0.1:80 HTTPS_PROXY=http://127.0.0.1:443 AWS_CA_BUNDLE="${HOME}/.iamlive/ca.pem"

docker cp iamlive-test:/home/appuser/.iamlive/ ~/
##  to check keys are there
ls ~/.iamlive

docker stop iamlive-test
docker start -i iamlive-test
docker exec iamlive-test kill -HUP 1 && docker exec iamlive-test cat /app/iamlive.log | jq

Try aws s3 ls and see permissions failed and the permissions required
In the permission tab for group add/create to policy
```json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "sts:GetCallerIdentity",
        "s3:CreateBucket",
        "s3:ListAllMyBuckets"
      ],
      "Resource": "*"
    }
  ]
}
```
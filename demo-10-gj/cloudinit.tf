data "template_file" "init-script" {
  template = file("scripts/init.cfg")
  vars = {
    REGION = var.AWS_REGION
  }
}

data "template_file" "shell-script" {
  template = file("scripts/volumes.sh")
  vars = {
    DEVICE = var.INSTANCE_DEVICE_NAME
  }
}

data "template_file" "docker-script" {
  template = file("scripts/dinst.sh")
  vars = {
    REGION = var.AWS_REGION
  }
}

data "template_cloudinit_config" "cloudinit-example" {
  gzip          = false
  base64_encode = false

  part {
    filename     = "init.cfg"
    content_type = "text/cloud-config"
    content      = data.template_file.init-script.rendered
  }

  part {
    content_type = "text/x-shellscript"
    content      = data.template_file.shell-script.rendered
  }

  part {
    content_type = "text/x-shellscript"
    content      = data.template_file.docker-script.rendered
  }

  depends_on = [aws_internet_gateway.main-gw]
}


# scale up alarm

resource "aws_autoscaling_policy" "example-cpu-policy-app1" {
  name                   = "example-cpu-policy-app1"
  autoscaling_group_name = aws_autoscaling_group.example-autoscaling-app1.name
  adjustment_type        = "ChangeInCapacity"
  scaling_adjustment     = "1"
  cooldown               = "300"
  policy_type            = "SimpleScaling"
}

resource "aws_cloudwatch_metric_alarm" "example-cpu-alarm-app1" {
  alarm_name          = "example-cpu-alarm-app1"
  alarm_description   = "example-cpu-alarm-app1"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "120"
  statistic           = "Average"
  threshold           = "30"

  dimensions = {
    "AutoScalingGroupName" = aws_autoscaling_group.example-autoscaling-app1.name
  }

  actions_enabled = true
  alarm_actions   = [aws_autoscaling_policy.example-cpu-policy-app1.arn]
}

# scale down alarm
resource "aws_autoscaling_policy" "example-cpu-policy-scaledown-app1" {
  name                   = "example-cpu-policy-scaledown-app1"
  autoscaling_group_name = aws_autoscaling_group.example-autoscaling-app1.name
  adjustment_type        = "ChangeInCapacity"
  scaling_adjustment     = "-1"
  cooldown               = "300"
  policy_type            = "SimpleScaling"
}

resource "aws_cloudwatch_metric_alarm" "example-cpu-alarm-scaledown-app1" {
  alarm_name          = "example-cpu-alarm-scaledown-app1"
  alarm_description   = "example-cpu-alarm-scaledown-app1"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "120"
  statistic           = "Average"
  threshold           = "5"

  dimensions = {
    "AutoScalingGroupName" = aws_autoscaling_group.example-autoscaling-app1.name
  }

  actions_enabled = true
  alarm_actions   = [aws_autoscaling_policy.example-cpu-policy-scaledown-app1.arn]
}


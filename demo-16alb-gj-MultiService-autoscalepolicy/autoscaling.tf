resource "aws_launch_configuration" "example-launchconfig-app1" {
  name_prefix     = "example-launchconfig-app1"
  image_id        = var.AMIS[var.AWS_REGION]
  instance_type   = "t2.micro"
  key_name        = aws_key_pair.mykeypair.key_name
  security_groups = [aws_security_group.myinstance.id]
  user_data       = "#!/bin/bash\nsudo -s\namazon-linux-extras install epel -y\nsudo yum install nginx -y\nMYIP=`ifconfig | grep 'inet 10' | awk '{ print $2 }'`\necho 'APP 1 : this is: '$MYIP > /usr/share/nginx/html/index.html\nsed -i '/:80/ a rewrite ^.*$ /index.html;' /etc/nginx/nginx.conf\nservice nginx start"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "example-autoscaling-app1" {
  name                      = "example-autoscaling-app1"
  vpc_zone_identifier       = [aws_subnet.main-public-1.id, aws_subnet.main-public-2.id, aws_subnet.main-public-3.id]
  launch_configuration      = aws_launch_configuration.example-launchconfig-app1.name
  min_size                  = 2
  max_size                  = 3
  health_check_grace_period = 300
  # These next two lines are the difference if you have an ELB with autoscaling compared to individual instances with autoscaling
  health_check_type         = "ELB"

  target_group_arns = [
    "${aws_alb_target_group.frontend-target-group-app1.arn}"
  ]


  force_delete              = true

  tag {
    key                 = "Name"
    value               = "ec2 instance 1"
    propagate_at_launch = true
  }
}

resource "aws_launch_configuration" "example-launchconfig-app2" {
  name_prefix     = "example-launchconfig-app2"
  image_id        = var.AMIS[var.AWS_REGION]
  instance_type   = "t2.micro"
  key_name        = aws_key_pair.mykeypair.key_name
  security_groups = [aws_security_group.myinstance.id]
  user_data       = "#!/bin/bash\nsudo -s\namazon-linux-extras install epel -y\nsudo yum install nginx -y\nMYIP=`ifconfig | grep 'inet 10' | awk '{ print $2 }'`\necho 'APP 2 : this is: '$MYIP > /usr/share/nginx/html/index.html\nsed -i '/:80/ a rewrite ^.*$ /index.html;' /etc/nginx/nginx.conf\nservice nginx start"

  lifecycle {
    create_before_destroy = true
  }
}


resource "aws_autoscaling_group" "example-autoscaling-app2" {
  name                      = "example-autoscaling-app2"
  vpc_zone_identifier       = [aws_subnet.main-public-1.id, aws_subnet.main-public-2.id]
  launch_configuration      = aws_launch_configuration.example-launchconfig-app2.name
  min_size                  = 2
  max_size                  = 5
  health_check_grace_period = 300
  health_check_type         = "ELB"
  target_group_arns = [
    "${aws_alb_target_group.frontend-target-group-app2.arn}"
  ]


  force_delete              = true

  tag {
    key                 = "Name"
    value               = "ec2 instance 2"
    propagate_at_launch = true
  }
}

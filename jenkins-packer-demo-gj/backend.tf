terraform {
  backend "s3" {
    bucket = "terraform-state-nmic6abn"
    key    = "terraform.tfstate"
    region = "eu-west-1"
  }
}

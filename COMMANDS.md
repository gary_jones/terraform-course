LOGS - can see logs in tail -f /var/log/cloud-init-output.log on the instance

PUBLIC/PRIVATE Keys - for all demos just use: ssh-keygen -f gjkey

CONNECT TO EC2 : ssh -i <privatekeyfile> ec2-user@<IP>

COMMANDS:
terraform init - initialise directory
terraform plan -out tf.plan.out - export the current plan from the .tf files in current directory to a file
terraform apply - apply the current plan from the .tf files in the current directory
terraform apply tf.plan.out - apply the plan save in a file
terraform show - show the current plan
terraform destroy - drop everything from the current applied infrastructure
terraform graph -module-depth=1 | dot -Tpng | open -a Preview -f
terraform output - display any output steps in script

terraform get - download any additional modules with remote "source" values to local .terraform/modules directory - e.g. see demo-6-gj/modules.tf

aws configure to set up local credentials for use by aws commands and other programs like terraform that need access to things like s3
aws configure --profile prod/dev etc. to setup a new profile


terraform plan - will show changes that will be applied to the current environment - 
ie. if nothing exists yet is should show you everything
once you have run apply, if you run plan again it shouls say "No changes. Infrastructure is up-to-date."
only changes applied to tf files will appear in plan after this so save them as files to be applied in a similar way to liquibase

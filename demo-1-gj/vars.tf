variable "AWS_ACCESS_KEY" {}
variable "AWS_SECRET_KEY" {}
variable "AWS_REGION" {
  default = "eu-west-2"
}

variable "AWS_PUBLIC_KEY" {
  default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDZhiEEMlCz0UvdxBQlxgLadLtKJCl904ZP2AAlfHITJGMZC8QF5YjLZn2NNGk+/n7Twm2qLIKDyA/TbkJ+J6DiRL46320uHI7l45swxN5CCrPPOUP1Loe9Me1zf4M/+F8IDjB0C448rK1D2Lptkfhwn0R8jbiguRHCv9ti3CycDnovmRbcg80i4Ffk4dBfQPGnp8BrY8hTC1UzQJY2SUtNhjP4U0J+p5d3cyozr7wPYdQiaRZ3M80/1Axecjk+1mi6vWL3lI2lY1UnwxvxnPiPp+oOT5MKxWrZbpb1OU2c0DJKgmIU6Zcde2cnx9fCqTIwJjucPTUoF+mPTgUWtB2F gary.jones@gj-mbp"
}

variable "AMIS" {
  type = map(string)
  default = {
    us-east-1 = "ami-13be557e"
    us-west-2 = "ami-06b94666"
    eu-west-1 = "ami-0d729a60"
    eu-west-2 = "ami-00a1270ce1e007c27"
  }
}


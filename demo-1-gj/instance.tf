# You can use an existing key but this section will upload a new public key if needed
resource "aws_key_pair" "gary" {
  key_name = "gary"
  public_key = var.AWS_PUBLIC_KEY
}

resource "aws_instance" "example" {
  ami           = var.AMIS[var.AWS_REGION]
  instance_type = "t2.micro"
  key_name = aws_key_pair.gary.key_name # Needed to associate instance with a key and allow ssh access
}

